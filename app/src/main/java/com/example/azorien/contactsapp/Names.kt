package com.example.azorien.contactsapp

class Names {
    val Random = java.util.Random()
    val names = listOf(
        "Norman Gordon",
        "Quiche Hollandaise",
        "Desmond Eagle",
        "Hans Down",
        "Eleanor Fant",
        "Penny Tool",
        "Chaplain Mondover",
        "Jason Response",
        "Benjamin Evalent",
        "Natalya Undergrowth",
        "Dylan Meringue",
        "Sir Cumference",
        "Gunther Beard",
        "Parsley Montana",
        "Shequondolisa Bivouac",
        "Burgundy Flemming",
        "Eric Widget",
        "Valentino Morose",
        "Gibson Montgomery-Gibson",
        "Archibald Northbottom",
        "Brandon Guidelines",
        "Giles Posture",
        "Ursula Gurnmeister",
        "Fleece Marigold",
        "Caspian Bellevedere"
    )

    fun getName() = names[Random.nextInt(names.size)]
    fun getNames(quantity: Int) = (1..quantity).map { getName() }
}