package com.example.azorien.contactsapp

import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView

class ContactsListAdapter(private val dataSet: ArrayList<Contact>) : RecyclerView.Adapter<ContactsListAdapter.ContactViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val constraintLayout = LayoutInflater.from(parent.context).inflate(R.layout.list_item, parent, false) as ConstraintLayout
        return ContactViewHolder(constraintLayout)
    }

    override fun onBindViewHolder(viewHolder: ContactViewHolder, position: Int) {
        viewHolder.avatarTextView.text = dataSet[position].name[0].toString()
        viewHolder.contactNameView.text = dataSet[position].name
    }

    override fun getItemCount() = dataSet.size

    fun removeAt(adapterPosition: Int) {
        dataSet.removeAt(adapterPosition)
        notifyItemRemoved(adapterPosition)
    }

    class ContactViewHolder(val layout: ConstraintLayout) : RecyclerView.ViewHolder(layout){
        val avatarTextView = layout.findViewById<TextView>(R.id.avatarText)
        val contactNameView = layout.findViewById<TextView>(R.id.contactName)
    }

}